$(window).load(function() {
	if(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
		$('body').addClass('ios');
	};
	$('body').removeClass('loaded');
});
/* viewport width */
function viewport() {
	var e = window,
		a = 'inner';
	if(!('innerWidth' in window)) {
		a = 'client';
		e = document.documentElement || document.body;
	}
	return {
		width: e[a + 'Width'],
		height: e[a + 'Height']
	}
};
var handler = function() {
	var height_footer = $('footer').outerHeight() + 24;
	if(!$('header').is('.fixed')) {
		var height_header = $('.header-inner').outerHeight();
	}
	$('.content').css({
		'padding-bottom': height_footer,
		'padding-top': height_header
	});


	var elemHeight = $('.js-aside-scroll').outerHeight();
	if($('.js-aside-scroll').length){

	var elemTopCoord = $('.js-aside-scroll').offset().top + $('.header-inner').outerHeight() - $(window).scrollTop();

	var elemBottomCoord = elemTopCoord + elemHeight;

	var bodyHeight = $('body').outerHeight();
	var temp;

	$('.rate-link-tab__link').click(function(){
		elemHeight = $('.js-aside-scroll').outerHeight();
		elemBottomCoord = elemTopCoord + elemHeight;
	});

	if (viewport().width > 991){
		var asideRightWidth = $('.wrapper-main>.row').outerWidth()/100*20.78-36;
	} else {
		var asideRightWidth = $('.wrapper-main>.row').outerWidth()/100*26.3-36;
	}

	$('.js-aside-scroll').css('width', asideRightWidth);

	var lastScrollTop = 0;
	// var tempElemTopCoord = $('.js-aside-scroll').offset().top+$('.header-inner').outerHeight()-$(window).scrollTop()-58;
	$(window).scroll(function(event){

		var wrapperHeight = $('.main-wrapper').outerHeight();
		var elemLeftCoord = $('.js-aside-scroll').offset().left;

		if (viewport().width < 768){
			elemLeftCoord = $('.col-left').innerWidth() + 15 + $('.col-middle').innerWidth();
		}

	   var st = $(this).scrollTop();
	   if (st > lastScrollTop){
	   		temp = $(window).scrollTop();
	       	if ($(window).scrollTop() < elemBottomCoord){
	       		$('.js-aside-scroll').css('position', '');
			}
			if($('.js-aside-scroll').hasClass('.js-aside-fixed')){
				$('.js-aside-scroll').css('margin-top', temp-94);
				$('.js-aside-scroll').css('position', '');
				$('.js-aside-scroll').removeClass('.js-aside-fixed');
			}
			if($(window).scrollTop()+bodyHeight+100 > wrapperHeight){
					$('.js-aside-scroll').css('position', '');
					$('.js-aside-scroll').css('margin-top', '5px');
				}
			if($(window).scrollTop() < 100){
					$('.js-aside-scroll').css('position', '');
					$('.js-aside-scroll').css('margin-top', '0px');	
				}
	   } else {

	   			

				if (($(window).scrollTop() < elemBottomCoord) && ($(window).scrollTop() < 100)){
		   			$('.js-aside-scroll').css('position', '');
		   			$('.js-aside-scroll').css('margin-top', '0px');
		   		}

				if($(window).scrollTop() < temp-elemBottomCoord+194){
					$('.js-aside-scroll').css('position', 'fixed');
					if (viewport().width < 768){
						$('.js-aside-scroll').css('right', '15px');
						$('.js-aside-scroll').css('left', '');
					} else {
						$('.js-aside-scroll').css('left', elemLeftCoord);
					}
					$('.js-aside-scroll').css('top', '58px');
					$('.js-aside-scroll').addClass('.js-aside-fixed');
					$('.js-aside-scroll').css('margin-top', '0px');				
				} else {
					$('.js-aside-scroll').css('position', '');
					$('.js-aside-scroll').css('margin-top', temp-elemBottomCoord+164);
				}
				if($(window).scrollTop() < 20){
					$('.js-aside-scroll').css('position', '');
				}
				if($(window).scrollTop()+bodyHeight+100 > wrapperHeight){
					$('.js-aside-scroll').css('position', '');
					$('.js-aside-scroll').css('margin-top', '5px');
				}
	   }
	   lastScrollTop = st;
	});
	}
}
$(window).bind('load', handler);
$(window).bind('resize', handler);
$(window).bind('orientationchange', handler);

$(function() {
	var viewport_wid = viewport().width;
	/* viewport width */
	var $win = $(window),
		menu = $('#menuList'),
		menuPanel = $('#mainNav'),
		mainNav = $('#mainNav'),
		header = $('header'),
		smWidth = 768;
	/* placeholder*/
	$('input, textarea').each(function() {
		var placeholder = $(this).attr('placeholder');
		$(this).focus(function() {
			$(this).attr('placeholder', '');
		});
		$(this).focusout(function() {
			$(this).attr('placeholder', placeholder);
		});
	});
	/* placeholder*/
	// fixed header
	if($('.js-fixed').length) {
		var el = $('.js-fixed'),
			offset_this = 0,
			scr_top = $win.scrollTop(),
			viewport_wid = viewport().width;
		if(scr_top > offset_this) {
			el.addClass("fixed");
		}
		else {
			el.removeClass("fixed");
		}
	}
	$win.scroll(function() {
		if($('.js-fixed').length) {
			scr_top = $(window).scrollTop();
			if(scr_top > offset_this) {
				el.addClass("fixed");
			}
			else {
				el.removeClass("fixed");
			}
		}
		// $(".js-scroll").each(function() {
		// 	var aside = $(this);
		// 	heightScrollbar(aside);
		// 	aside.mCustomScrollbar('update');
		// });
	});
	// width scrollbar
	function getScrollBarWidth() {
		var $outer = $('<div>').css({
				visibility: 'hidden',
				width: 100,
				overflow: 'scroll'
			}).appendTo('body'),
			widthWithScroll = $('<div>').css({
				width: '100%'
			}).appendTo($outer).outerWidth();
		$outer.remove();
		return 100 - widthWithScroll;
	};
	var scrollbarWidth = getScrollBarWidth();
	if(!(window.ActiveXObject) && "ActiveXObject" in window) {
		$('body').addClass('ie-11');
		scrollbarWidth = 0;
	}
	var slider = $("#slider");
	if(slider.length > 0) {
		var sudoSlider = slider.sudoSlider({
			continuous: true,
			touch: true,
			mouseTouch: true,
			touchHandle: "> * :not(a)",
			prevNext: false,
			numeric: true
		});
	}
	//sticky nav
	var leftTop = 0;

	function stikyLeftSide(scrollSide, contentCol) {
		if(viewport().width >= 768 && !($('#loginHide').is(':visible') && $('.main-content').outerHeight() <= $('.aside-right-inner').outerHeight() + $(window).scrollTop())) {
			var scrollPos = $win.scrollTop();
			var width = scrollSide.parent('.sticky-nav-wrap').width();
			scrollSide.css({
				'width': width,
				'margin-top': ''
			});
			if(scrollPos > leftTop && scrollPos < (leftTop + contentCol.outerHeight() - scrollSide.outerHeight())) {
				scrollSide.css({
					'top': $('.header-inner').outerHeight()
				}).addClass('fixed').removeClass('relative bottom');
			}
			else if(scrollPos <= leftTop) {
				setTimeout(function() {
					scrollSide.removeClass('fixed').css({
						'top': 0
					}).addClass('relative');
				}, 1);
			}
			else if(scrollPos > leftTop && scrollPos >= (leftTop + contentCol.outerHeight() - scrollSide.outerHeight())) {
				scrollSide.addClass('bottom').removeClass('fixed').css({
					'top': ''
				});
			}
		}
		else {
			if(viewport().width < 768) {
				$('.js-sticky-nav').each(function() {
					$(this).removeClass('relative fixed bottom').css({
						'margin-top': ''
					});
				});
			}
			if($('#loginHide').is(':visible') && $('.main-content').outerHeight() <= $('.aside-right-inner').outerHeight() + $(window).scrollTop()) {
				$('.aside-right').find('.js-sticky-nav').removeClass('fixed bottom').addClass('relative').css({
					'margin-top': ''
				});
			}
		}
	}


	$('.event-more-information').click(function(){
		$(this).parents('.results-list__description').toggleClass('active');
	});







	$('.js-sticky-nav').each(function() {
		var scrollBlock = $(this);
		var leftTop = scrollBlock.closest('.js-sticky-wrap');
		stikyLeftSide(scrollBlock, leftTop);
		$(window).on('scroll', function() {
			stikyLeftSide(scrollBlock, leftTop);
		});
	});

	function heightScrollbar(aside) {
		var el = $(aside),
			parents = el.closest('.js-sticky-nav'),
			winHeight = viewport().height,
			staticHeight = parents.find('.aside-inner').outerHeight() - el.outerHeight(),
			offsetTop = $('.header-inner').outerHeight(),
			offset = 0,
			maxHeigh,
			heightLogin = 0,
			scrollTop = $win.scrollTop();
		if(scrollTop < 1) {
			offsetTop = $('.header-inner').outerHeight() + parseFloat($('.js-sticky-wrap').css('padding-top'));
		}
		if(el.closest('.sticky-nav-wrap').is('.aside-right') && parents.find('#loginHide').length && parents.find('#loginHide').is(':visible')) {
			heightLogin = $('#loginHide').height();
		}
		if(scrollTop + winHeight > $('footer').offset().top) {
			offset = scrollTop + winHeight - $('footer').offset().top + 24;
		}
		maxHeigh = winHeight - offsetTop - offset - staticHeight + heightLogin;
		if(maxHeigh < 32) maxHeigh = 32;
		// if(maxHeigh >= 460) {
		// 	maxHeigh = 460;
		// }
		el.css('max-height', maxHeigh);
	}





	// $(window).on("load", function() {
	// 	$(".js-scroll").each(function() {
	// 		var aside = $(this);
	// 		// heightScrollbar(aside);
	// 		// aside.mCustomScrollbar({
	// 		// 	setHeight: false,
	// 		// 	snapAmount: 500,
	// 		// 	scrollInertia: 30,
	// 		// 	mouseWheel:{ scrollAmount: 500 }
	// 		// });
	// 	});
	// });





	$(window).on('resize', function() {
		// $(".js-scroll").each(function() {
		// 	var aside = $(this);
		// 	heightScrollbar(aside);
		// });
		$('.js-sticky-nav').each(function() {
			var scrollBlock = $(this);
			var leftTop = scrollBlock.closest('.js-sticky-wrap');
			stikyLeftSide(scrollBlock, leftTop);
		});
		if($('.popups').is('.open')) {
			$('.popups.open').positionPopup();
		}
	});

	function replaceLoginValue() {
		if($('.login-name span, .lk').length) {
			var el = $('.login-name span');
			if(!$('.lk').is('.open')) {
				var text = el.text();
				text = text.replace(/(.?)/g, '*');
				el.text(text);
			}
			else {
				el.text(el.closest('.login-name').data('title'));
			}
		}
	}
	replaceLoginValue();
	$('.js-toggle-block').click(function() {
		var el = $(this),
			text1 = el.data('close-text'),
			text2 = el.data('open-text'),
			blockSlide = $(el.attr('href'));
		el.text(blockSlide.is(":visible") ? text2 : text1);
		if(el.is('.js-toggle-block-not-animate')) {
			blockSlide.toggle();
		}
		else {
			blockSlide.slideToggle(300);
		}
		if(el.is('#linkToggleLogin')) {
			var scrollBlock = $('.aside-right').find('.js-sticky-nav'),
				leftTop = scrollBlock.closest('.js-sticky-wrap'),
				aside = scrollBlock.find(".js-scroll");
			stikyLeftSide(scrollBlock, leftTop);
			heightScrollbar(aside);
			el.closest('.lk').toggleClass('open');
			if($('#loginHide').is(':visible') && $('.main-content').outerHeight() <= $('.aside-right-inner').outerHeight() + $(window).scrollTop()) {
				var offset = $win.scrollTop() - $('.header-inner').outerHeight();
				if(offset < 0) {
					offset = 0;
				}
				$('.aside-right').find('.js-sticky-nav').css({
					'margin-top': offset
				});
			}
			replaceLoginValue();
		}
		blockSlide.find('.styled').each(function() {
			$(this).trigger('refresh');
		});
		if(el.is('.menu-aside__link')) {
			// el.prev('.js-scroll').mCustomScrollbar("scrollTo", 0, {
			// 	scrollInertia: 0
			// });
		}
		return false;
	});
	$.fn.positionPopup = function() {
			var height = this.outerHeight(),
				winHeight = viewport().height;
			if(height <= winHeight) {
				this.css({
					'position': 'fixed',
					'top': (winHeight - height) / 2
				});
			}
			else {
				this.css({
					'position': 'absolute',
					'top': $win.scrollTop()
				});
			}
		}
		// open popup
	$(document).on('click', '.js-link-popup', function() {
		var el = $(this).attr('href');
		$(el).addClass('open').positionPopup();
		if($('body').is('.ie-11')) {
			// blur ie
			$('main, footer, header').foggy();
		}
		else {
			$('main, footer, header').addClass('blur');
		}
		return false;
	});
	// close block
	$('.js-close-block').click(function() {
		var el = $(this).closest('.js-block-close');
		el.removeClass('open').css({
			'position': '',
			'top': ''
		});
		if($('body').is('.ie-11')) {
			// blur ie
			$('main, footer, header').foggy(false);
		}
		else {
			$('main, footer, header').removeClass('blur');
		}
		return false;
	});
	$('body').on("click touchstart", function(e) {
		if($(e.target).closest('.popups').length) {
			return;
		}
		else {
			if($('.popups').is('.open')) {
				$('.popups').removeClass('open').css({
					'position': '',
					'top': ''
				});
				if($('body').is('.ie-11')) {
					// blur ie
					$('main, footer, header').foggy(false);
				}
				else {
					$('main, footer, header').removeClass('blur');
				}
				$("html").css({
					'overflow': '',
					'padding-right': ''
				});
			}
		}
	});
	// disabled tab
	if($('.js-message').length) {
		$('.rate-link-tab').find('li').removeClass('active').find('a').addClass('disabled');
	}
	// tabs
	$('.js-tab li a').click(function() {
		var el = $(this),
			id = el.attr('href');
		if($('.js-message').length) {
			// disabled tab
			return false;
		}
		if(el.closest('#rateBlock').length && el.closest('#rateBlock').is('.on')) {
			$('#fastRateInput').trigger('click');
		}
		el.closest('.tab-wrap').find('.tab-cont').hide();
		el.parent().addClass('active').siblings().removeClass('active');
		$(id).show();
		$(id).find('.styled').each(function() {
			$(this).trigger('refresh');
		});
		return false;
	});
	// events input fast Rate
	$('#fastRateInput').change(function() {
		var el = $(this),
			elem = el.closest('#rateBlock').find('.js-tab');
		if($(this).is(':checked')) {
			$('#rateBlock').addClass('on');
			elem.find('li').removeClass('active');
			el.closest('#rateBlock').find('.tab-cont').removeClass('active');
		}
		else {
			$('#rateBlock').removeClass('on');
			elem.find('li').first().addClass('active');
			el.closest('#rateBlock').find('.tab-cont').first().addClass('active');
		}
		// var aside = $('.aside-right').find(".js-scroll");
		// heightScrollbar(aside);
	});
	if($('#numberCard').length) {
		$("#numberCard").mask("9999 9999 9999 9999");
	}
	// filters messages
	$('.js-filter').click(function() {
			var el = $(this),
				href = el.data('href-link');
			$('.js-filter').removeClass('active');
			$('.message-list__item').hide();
			$('.' + href + '').fadeIn();
			el.addClass('active');
			return false;
		})
		// accordion
	$('.accordion__head').on('click', function() {
		var el = $(this);
		el.next('.accordion__body').slideToggle();
		el.toggleClass('open');
		return false;
	});
	// messages accordion
	$('.js-dropdown').click(function() {
		var el = $(this);
		el.toggleClass('active').next('.dropdown-content').slideToggle();
		if(el.closest('.message-list__item').is('.message-list__item_new')) {
			el.closest('.message-list__item').removeClass('message-list__item_new');
		}
	});
	// history accordion
	$('.js-accordion').click(function() {
		var el = $(this);
		el.toggleClass('active').closest('.js-head').find('.js-content').slideToggle();
	});
	var popupSlider = $(".sliders");
	if(popupSlider.length > 0) {
		popupSlider.slick({
			dots: true,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 9000,
			speed: 1000,
			slidesToShow: 1,
			slidesToScroll: 1,
			touchThreshold: 200,
			arrows: false
		});
	}
	// styled elements forms
	if($('.styled').length) {
		$('.styled').styler();
		// event onchange select
		$('select').change(function() {});
	}
	// remove block
	$('.js-remove-item').on('click', function() {
		var el = $(this);
		el.closest('.js-remove-block').remove();
	});
	// like icon event
	$('.unlike-icon').on('click', function() {
		$(this).removeClass('unlike-icon');
	});
});